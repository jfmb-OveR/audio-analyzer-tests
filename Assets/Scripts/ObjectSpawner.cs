﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    [SerializeField]
    AudioAnalyzer _myAudioAnalyzer;
    [SerializeField]
    GameObject[] aObjects;
    [SerializeField]
    float fThreshold = 1;
    [SerializeField]
    int iBPM = 60;

    List<GameObject>[] aAllListOfObjects;

    float fTimer = 0;
    float fBPS = 0;
    
    void OnTriggerEnter(Collider otherCollider){
//        Debug.Log("COllision!!!! " + otherCollider.transform.parent.name);
        otherCollider.transform.parent.gameObject.SetActive(false);
//        otherCollider.transform.position = new Vector3 (otherCollider.transform.parent.trasnform.position.x,
//                                                        otherCollider.transform.parent.trasnform.position.y,
//                                                        otherCollider.transform.parent.trasnform.position.z);
        int iIndex = int.Parse(otherCollider.transform.parent.name);
        aAllListOfObjects[iIndex].Add(otherCollider.transform.parent.gameObject);
//        Debug.Log("Añadiendo objeto");
    }

    public void SpawnObjects(int iIndex){
//        if(_myAudioAnalyzer.GetBufferFreqBand2FromIndex(iIndex) > fThreshold){
        if(_myAudioAnalyzer.GetAudioBandBufferFromIndex(iIndex) > fThreshold){
            if(aAllListOfObjects[iIndex].Count > 0){
                aAllListOfObjects[iIndex][0].SetActive(true);
                aAllListOfObjects[iIndex].RemoveAt(0);
//                Debug.Log("Borrando objeto");
            }
            else{
                GameObject goTemp = Instantiate(aObjects[iIndex], this.transform);
                goTemp.transform.position = new Vector3 (aObjects[iIndex].transform.position.x,
                                                        aObjects[iIndex].transform.position.y,
                                                        aObjects[iIndex].transform.position.z);
                goTemp.SetActive(true);
                goTemp.name = iIndex.ToString();
                aAllListOfObjects[iIndex].Add(goTemp);
            }
        }  
//        for(int i = 0; i < aAllListOfObjects.Length; i++){
//            Debug.Log("Lista " + i + ": " + aAllListOfObjects[i].Count);
//        }
    }

    void OnDestroy(){
        for(int i = 0; i < aAllListOfObjects.Length; i++){
            for(int j = 0; j < aAllListOfObjects[i].Count; j++){
                GameObject goTemp = aAllListOfObjects[i][j];
                aAllListOfObjects[i].RemoveAt(j);
                Destroy(goTemp);
            }
        }
    }

    void Start()
    {
        aAllListOfObjects = new List<GameObject>[aObjects.Length];
        for(int i = 0; i < aAllListOfObjects.Length; i++){
            aAllListOfObjects[i] = new List<GameObject>();
        }

        fBPS = 1/(float)(iBPM/60);

        Debug.Log("Beats por segundo: " + fBPS);
        for(int i = 0; i < aObjects.Length; i++){
            aObjects[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        fTimer += Time.deltaTime;
        if(fTimer >= fBPS){
            Debug.Log("Beat!");
            fTimer = 0;
            SpawnObjects(Random.Range(0, aObjects.Length));
        }
    }
}
