﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/***
This analyzer is based on this tutorial: 
https://www.youtube.com/watch?v=5pmoP1ZOoNs
by Peer Play
***/

[RequireComponent(typeof(AudioSource))]
public class AudioAnalyzer : MonoBehaviour
{
    [SerializeField]
    float[] aSamples = new float[512];
    [SerializeField]
    float[] aFreqBand = new float[8];
    [SerializeField]
    float fDecreaseTimeStep = 5f;



    float[] aBufferFreqBand = new float[8];
    float[] aBufferFreqBand2 = new float[8];
    float[] aBufferDecrease = new float[8];

    float[] aAudioBand = new float[8];
    float[] aAudioBandBuffer = new float[8];
    float[] aFreqBandHighest = new float[8];

    AudioSource audioMySource;

    public float GetValueFromIndex(int iIndex)
    {
        return aSamples[iIndex];
    }

    public float GetFreqBandFromIndex(int iIndex){
        return aFreqBand[iIndex];
    }
    public float GetBufferFreqBandFromIndex(int iIndex){
        return aBufferFreqBand[iIndex];
    }
    public float GetBufferFreqBand2FromIndex(int iIndex){
        return aBufferFreqBand2[iIndex];
    }

    public float GetAudioBandFromIndex(int iIndex){
        return aAudioBand[iIndex];
    }

    public float GetAudioBandBufferFromIndex(int iIndex){
        return aAudioBandBuffer[iIndex];
    }

    void GetSpectrumAudioSource(){
        audioMySource.GetSpectrumData(aSamples, 0, FFTWindow.Blackman);
    }

    void NormalizeAudioBands(){
        //Normalize values bettween 0 - 1
        for(int i = 0; i < aAudioBand.Length; i++){
            if(aFreqBand[i] > aFreqBandHighest[i])
                aFreqBandHighest[i] = aFreqBand[i];
            
            aAudioBand[i] = (aFreqBand[i] / aFreqBandHighest[i]);
            aAudioBandBuffer[i] = (aAudioBand[i] / aFreqBandHighest[i]);
        }
    }

    /***
    * 22050 Hz (max freq.)/ 512 bands = 43 hertz per sample
    *  7 Channels (or bands):
    * 20 - 60 Hertz
        60 - 250 
        250 - 500
        500 - 2000
        2000 - 4000
        4000 - 6000
        6000 - 20000
    *
    *   0: 2 samples (deep bass) = 86 Hz -> 43 - 86 Hz
        1: 4 samples = 172 Hz -> 87 - 258 Hz
        2: 8 samples = 344 Hz -> 259 - 602 Hz
        3: 16 samples = 688 Hz -> 603 - 1290 Hz
        4: 32 samples = 1376 Hz -> 1291 - 2666 Hz
        5: 64 samples = 2752 Hz -> 2667 - 5418 Hz
        6: 128 samples = 5504 Hz -> 5419 - 10922 Hz
        7: 256 samples = 11008 Hz -> 10923 - 21930 Hz
        Total: 510 -> we'll add 2 more at the end of the loop to achieve 22050 HZ
    *
    */
    void MakeFrequencyBands(){
        int iCount = 0;
        for (int i = 0; i < 8; i++){
            float fAverage = 0;
            int iSampeCount = (int)Mathf.Pow(2, i) * 2;

            if( i == 7) //We add 2 to achieve 512
                iSampeCount += 2;

            for(int j = 0; j < iSampeCount; j++){
                fAverage += aSamples[iCount] * (iCount +1);
                iCount++;
            }

            fAverage /= iCount;
            aFreqBand[i] = fAverage * 10;
        }
    }

    void RefreshBuffer(){
        for(int i = 0; i < aBufferFreqBand.Length; i++){
            if(aFreqBand[i] >= aBufferFreqBand[i]){
                aBufferFreqBand[i] = aFreqBand[i];
                aBufferFreqBand2[i] = aFreqBand[i];
                aBufferDecrease[i] = 0.005f;
            }
            else{
                aBufferFreqBand[i] -= Time.deltaTime * fDecreaseTimeStep; //Method 1: smooth value with time

                aBufferFreqBand2[i] -= aBufferDecrease[i]; //Method 2: smooth value with a Buffer Decrease
                aBufferDecrease[i] *= 1.2f;

                if(aBufferFreqBand2[i] < 0)
                    aBufferFreqBand2[i] = 0;
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        audioMySource = GetComponent<AudioSource>();

        for(int i = 0; i < aFreqBand.Length; i++){
            aBufferFreqBand[i]= aBufferFreqBand2[i] = aFreqBand[i] = aFreqBandHighest[i] = aAudioBand[i] = aAudioBandBuffer[i] = 0f;
            aBufferDecrease[i] = 0.005f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        GetSpectrumAudioSource();
        MakeFrequencyBands();
        RefreshBuffer();
        NormalizeAudioBands();
    }
}
