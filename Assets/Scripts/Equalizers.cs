﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equalizers : MonoBehaviour
{
    [SerializeField]
    GameObject goEqualizer;
    [SerializeField]
    AudioAnalyzer _myAudioAnalyzer;

    GameObject[] aEqualizers = new GameObject[512];
    const float fConstAnglePortion = 512 / 360;

    void RefreshEqualizerValues(){
        for(int i = 0; i < 512; i++){
            aEqualizers[i].transform.localScale = new Vector3(aEqualizers[i].transform.localScale.x,
                                                                _myAudioAnalyzer.GetValueFromIndex(i) * 1000,
                                                                aEqualizers[i].transform.localScale.z);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 512; i++){
            aEqualizers[i] =  Instantiate(goEqualizer, this.transform);
            aEqualizers[i].transform.position = new Vector3(this.transform.position.x + 100,
                                                            this.transform.position.y,
                                                            this.transform.position.z);

            this.transform.eulerAngles = new Vector3(0, -fConstAnglePortion * i, 0);
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        RefreshEqualizerValues();
    }
}
