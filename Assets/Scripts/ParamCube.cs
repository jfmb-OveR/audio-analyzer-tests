﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParamCube : MonoBehaviour
{
    [SerializeField]
    AudioAnalyzer _myAudioAnalyzer;
    [SerializeField]
    int iBand;
    [SerializeField]
    float fStartScale = 1f;
    [SerializeField]
    float fScaleMultipier = 10f;
    [SerializeField]
    bool bBufferTest = true;

    Material _myMaterial;
    Color _myColor;
    float fValueY = 0f;
    // Update is called once per frame
    float fColorValue = 0f;

    void Start(){
        _myMaterial = this.GetComponentInChildren<MeshRenderer>().materials[0];
    }

    void Update()
    {
        if(bBufferTest){ //Normalized values
            fValueY = (_myAudioAnalyzer.GetBufferFreqBandFromIndex(iBand) * fScaleMultipier) + fStartScale;
//            fValueY = (_myAudioAnalyzer.GetAudioBandBufferFromIndex(iBand) * fScaleMultipier) + fStartScale;

        }
        else{
            fValueY = (_myAudioAnalyzer.GetBufferFreqBand2FromIndex(iBand) * fScaleMultipier) + fStartScale;
        }

        if(fValueY < 0)
            fValueY = 0;

        fColorValue = _myAudioAnalyzer.GetAudioBandBufferFromIndex(iBand);    
        _myColor = new Color(fColorValue, fColorValue, fColorValue);
        _myMaterial.SetColor("_EmissionColor", _myColor);
        transform.localScale = new Vector3(3, fValueY, 3);
    }
}
